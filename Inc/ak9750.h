/*
 * ak9750.h
 *
 *  Created on: Jan 18, 2019
 *      Author: tarek
 */

#ifndef AK9750_H_
#define AK9750_H_

#include "stdbool.h"
#include "stm32l0xx_hal.h"

extern I2C_HandleTypeDef hi2c1;
#define AK975X_ADDRESS           (0x66<<1)

//Register addresses
#define AK975X_WIA1               0x00
#define AK975X_WIA2               0x01 //Device ID
#define AK975X_INFO1              0x02
#define AK975X_INFO2              0x03
#define AK975X_INTST              0x04
#define AK975X_ST1                0x05
#define AK975X_IR1                0x06
#define AK975X_IR2                0x08
#define AK975X_IR3                0x0A
#define AK975X_IR4                0x0C
#define AK975X_TMP                0x0E
#define AK975X_ST2                0x10 //Dummy register
#define AK975X_ETH13H_LOW         0x11
#define AK975X_ETH13H_HIGH        0x12
#define AK975X_ETH13L_LOW         0x13
#define AK975X_ETH13L_HIGH        0x14
#define AK975X_ETH24H_LOW         0x15
#define AK975X_ETH24H_HIGH        0x16
#define AK975X_ETH24L_LOW         0x17
#define AK975X_ETH24L_HIGH        0x18
#define AK975X_EHYS13             0x19
#define AK975X_EHYS24             0x1A
#define AK975X_EINTEN             0x1B
#define AK975X_ECNTL1             0x1C
#define AK975X_CNTL2              0x19
#define AK975X_EKEY               0x50

//Valid sensor modes - Register ECNTL1
#define AK975X_MODE_STANDBY       0
#define AK975X_MODE_EEPROM_ACCESS 1
#define AK975X_MODE_SINGLE_SHOT   2
#define AK975X_MODE_0             4
#define AK975X_MODE_1             5
#define AK975X_MODE_2             6
#define AK975X_MODE_3             7

//Valid digital filter cutoff frequencies
#define AK975X_FREQ_0_3HZ         0
#define AK975X_FREQ_0_6HZ         1
#define AK975X_FREQ_1_1HZ         2
#define AK975X_FREQ_2_2HZ         3
#define AK975X_FREQ_4_4HZ         4
#define AK975X_FREQ_8_8HZ         5

//EEPROM addresses
#define AK975X_EETH13H_LOW        0X51
#define AK975X_EETH13H_HIGH       0X52
#define AK975X_EETH13L_LOW        0X53
#define AK975X_EETH13L_HIGH       0X54
#define AK975X_EETH24H_LOW        0X55
#define AK975X_EETH24H_HIGH       0X56
#define AK975X_EETH24L_LOW        0X57
#define AK975X_EETH24L_HIGH       0X58
#define AK975X_EEHYS13            0X59
#define AK975X_EEHYS24            0X5A
#define AK975X_EEINTEN            0X5B

//EEPROM functions
#define EEPROM_MODE               0xC1
#define EKEY_ON                   0xA5 // 0b10100101=0xA5

_Bool ak9750_init(void);
_Bool ak9750_reboot(void);
int16_t ak9750_get_ir1(void);
int16_t ak9750_get_ir2(void);
int16_t ak9750_get_ir3(void);
int16_t ak9750_get_ir4(void);
void    ak9750_refresh(void);
float   ak9750_get_temperature(void);

void ak9750_set_mode(uint8_t mode);
void ak9750_set_cutoff_frequency(uint8_t frequency);

_Bool ak9750_set_threshold_ir2ir4(bool weight, uint16_t v);
_Bool ak9750_set_threshold_eeprom_ir2ir4(bool weight, uint16_t v);

_Bool ak9750_set_threshold_ir1ir3(bool weight, uint16_t v);
_Bool ak9750_set_threshold_eeprom_ir1ir3(bool weight, uint16_t v);

void ak9750_read_threshold(uint8_t* thresholds);
void ak9750_read_threshold_eeprom(uint8_t* thresholds);

_Bool ak9750_set_hysteresis_Ir2Ir4(uint8_t v);
_Bool ak9750_set_hysteresis_eeprom_ir2ir4(uint8_t v);

_Bool ak9750_set_hysteresis_ir1ir3(uint8_t v);
_Bool ak9750_set_hysteresis_eeprom_ir1ir3(uint8_t v);

void ak9750_read_hysteresis(uint8_t* hysteresis);
void ak9750_read_hysteresis_reproms(uint8_t* hysteresis);

_Bool ak9750_setInterrupts(bool ir13h,bool ir13l,bool ir24h,bool ir24l,bool dr);
uint8_t ak9750_readInterruptStatus(void);

_Bool ak9750_available_check(void);

_Bool ak9750_overrun_check(void);

void ak9750_soft_reset(void);

_Bool ak9750_check_ready(void);


uint8_t  ak9750_read_reg(uint8_t reg);
uint16_t ak9750_read_reg16(uint8_t reg);
void     ak9750_write_reg(uint8_t reg, uint8_t data);
void     ak9750_get_data(int16_t* data);




#endif /* AK9750_H_ */
