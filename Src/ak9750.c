/*
 * ak9750.c
 *
 *  Created on: Jan 18, 2019
 *      Author: tarek
 */

#include "main.h"
#include "stm32l0xx_hal.h"
#include "stdbool.h"
#include "ak9750.h"

//==============================================================================================
// reads 8 bit register
uint8_t ak9750_read_reg(uint8_t reg)
{
	uint8_t result;
	HAL_StatusTypeDef errors;
	errors = HAL_I2C_Master_Transmit(&hi2c1, AK975X_ADDRESS, &reg, 1,1000);
	if (errors != HAL_OK)
	{
		return 0;
	}
	errors = HAL_I2C_Master_Receive(&hi2c1, AK975X_ADDRESS, &result, 1,1000);
	if (errors != HAL_OK)
	{
		return 0;
	}
	return result;
}
//==============================================================================================
// reads 16 bit register
uint16_t ak9750_read_reg16(uint8_t reg)
{
	uint8_t result[2];
	HAL_StatusTypeDef errors;
	errors = HAL_I2C_Master_Transmit(&hi2c1, AK975X_ADDRESS, &reg, 1, 1);
	if (errors != HAL_OK)
	{
		return 0;
	}
	errors = HAL_I2C_Master_Receive(&hi2c1, AK975X_ADDRESS, result, 2, 1);
	if (errors != HAL_OK)
	{
		return 0;
	}
	return ((uint16_t) result[0]) | (((uint16_t) result[1]) << 8);
}
//==============================================================================================
// writes data to 8 bit register
void ak9750_write_reg(uint8_t reg, uint8_t data)
{
	uint8_t to_transmit[2];
	HAL_StatusTypeDef errors;
	to_transmit[0] = reg;
	to_transmit[1] = data;
	errors = HAL_I2C_Master_Transmit(&hi2c1, AK975X_ADDRESS, to_transmit,2, 2);
	if (errors != HAL_OK)
	{
		return;
	}
}
//==============================================================================================
_Bool ak9750_init(void)
{
	static uint8_t device_id = 0;
	HAL_GPIO_WritePin(AK9750_PWD_GPIO_Port, AK9750_PWD_Pin, GPIO_PIN_SET);
	HAL_Delay(500);
	if(HAL_I2C_IsDeviceReady(&hi2c1, AK975X_ADDRESS, 10, 1000)!= HAL_OK)
	   {
		   return false;
	   }
	device_id = ak9750_read_reg(AK975X_WIA2);            // should be 0x13
	if(device_id != 0x13)                                //Device ID should be 0x13
		return false;
	ak9750_set_mode(AK975X_MODE_0);           //Set to continuous read
	ak9750_set_cutoff_frequency(AK975X_FREQ_0_6HZ);
	ak9750_refresh();                                   //Read dummy register after new data is read
	return true;
}	
//==============================================================================================
_Bool ak9750_reboot()
{
	ak9750_set_mode(AK975X_MODE_0);                 //Set to continuous read
	ak9750_set_cutoff_frequency(AK975X_FREQ_0_3HZ); //Set output to fastest, with least filtering
	ak9750_refresh();                               //Read dummy register after new data is read
	return 1;                                       //Success!
}
//==============================================================================================
int16_t ak9750_get_ir1()
{
	return (ak9750_read_reg16(AK975X_IR1));
}
//==============================================================================================
int16_t ak9750_get_ir2()
{
	return (ak9750_read_reg16(AK975X_IR2));
}
//==============================================================================================
int16_t ak9750_get_ir3()
{
	return (ak9750_read_reg16(AK975X_IR3));
}
//==============================================================================================
int16_t ak9750_get_ir4()
{
	return (ak9750_read_reg16(AK975X_IR4));
}
//==============================================================================================
//This reads the dummy register ST2. Required after
//reading out IR data.
void ak9750_refresh()
{
	ak9750_read_reg(AK975X_ST2); //Do nothing but read the register
}
//==============================================================================================
//Returns the temperature in C
float ak9750_get_temperature()
{
	int16_t value = ak9750_read_reg16(AK975X_TMP);
	value >>= 6; //Temp is 10-bit. TMPL0:5 fixed at 0
	float temperature = 26.75 + (value * 0.125);
	return temperature;
}
//==============================================================================================
//Set the mode. Continuous mode 0 is favored
void ak9750_set_mode(uint8_t mode)
{
	if (mode > AK975X_MODE_3)
		mode = AK975X_MODE_0; //Default to mode 0
	if (mode == 0x03)
		mode = AK975X_MODE_0; //0x03 is prohibited

	//Read, mask set, write
	uint8_t current_settings = ak9750_read_reg(AK975X_ECNTL1);

	current_settings &= 0xF8; //Clear Mode bits
	current_settings |= mode;
	ak9750_write_reg(AK975X_ECNTL1, current_settings);
}
//==============================================================================================
//Set the cutoff frequency. See datasheet for allowable settings.
void ak9750_set_cutoff_frequency(uint8_t frequency)
{
	if (frequency > 0x05)
		frequency = 0; //Default to 0.3Hz

	//Read, mask set, write
	uint8_t current_settings = ak9750_read_reg(AK975X_ECNTL1);

	current_settings &= 0xC7;             //Clear EFC bits
	current_settings |= (frequency << 3); //Mask in
	ak9750_write_reg(AK975X_ECNTL1, current_settings); //Write
}
//==============================================================================================
_Bool  ak9750_setInterrupts(_Bool  ir13h, _Bool  ir13l, _Bool  ir24h, _Bool  ir24l,_Bool  dr) 
{
	// mask , assembly of AK975X_EINTEN bits
	uint8_t v = 0xC0;
	_Bool  result = 1;
	v |= ((uint8_t) ir13h << 4 | (uint8_t) ir13l << 3);
	v |= ((uint8_t) ir24h << 2 | (uint8_t) ir24l << 1 | (uint8_t) dr);

	ak9750_write_reg(AK975X_EINTEN, v);

	uint8_t current_settings = ak9750_read_reg(AK975X_EINTEN);
	result &= (current_settings == v);
	return result;
}
//==============================================================================================
uint8_t ak9750_readInterruptStatus()
{
	uint8_t current_settings = ak9750_read_reg(AK975X_INTST);
	return current_settings;
}
//==============================================================================================
//Checks to see if DRDY flag is set in the status register
_Bool  ak9750_available_check()
{
	uint8_t value = ak9750_read_reg(AK975X_ST1);
	return (value & (1 << 0)) == 1;
}
//==============================================================================================
//Checks to see if Data overrun flag is set in the status register
_Bool  ak9750_overrun_check()
{
	uint8_t value = ak9750_read_reg(AK975X_ST1);
	return (value & (1 << 1)) == 1;
}
//==============================================================================================
//Does a soft reset
void ak9750_soft_reset()
{
	ak9750_write_reg(AK975X_CNTL2, 0xff);
}
//==============================================================================================
_Bool  ak9750_check_ready() 
{
	HAL_StatusTypeDef result;
	result = HAL_I2C_IsDeviceReady(&hi2c1, AK975X_ADDRESS, 5, 5);
	if (result == HAL_OK)
	{
		return 1;
	}
	else
	{
		return 0;
	}
}
//==============================================================================================
void ak9750_get_data(int16_t* data)
{
	data[0] = ak9750_get_ir1();
	data[1] = ak9750_get_ir2();
	data[2] = ak9750_get_ir3();
	data[3] = ak9750_get_ir4();
}
